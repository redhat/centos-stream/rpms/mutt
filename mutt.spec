%bcond_without debug
%bcond_without imap
%bcond_without pop
%bcond_without smtp
%bcond_without gnutls
%bcond_without gss
%bcond_without sasl
%bcond_with idn
%bcond_without idn2
%bcond_without hcache
%bcond_without tokyocabinet
%bcond_with bdb
%bcond_with qdbm
%bcond_with gdbm
%bcond_without gpgme
%bcond_without sidebar

%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}

Summary: A text mode mail user agent
Name: mutt
Version: 2.2.13
Release: 4%{?dist}
Epoch: 5
# The entire source code is GPLv2+ except
# pgpewrap.c setenv.c sha1.c wcwidth.c which are Public Domain
License: GPL-2.0-or-later AND LicenseRef-Fedora-Public-Domain
# hg snapshot created from http://dev.mutt.org/hg/mutt
Source: http://ftp.mutt.org/pub/%{name}/%{name}-%{version}.tar.gz
Source1: mutt_ldap_query
Patch1: mutt-1.10.0-muttrc.patch
Patch2: mutt-1.8.0-cabundle.patch
# https://dev.mutt.org/trac/ticket/3569
Patch3: mutt-1.7.0-syncdebug.patch
# FIXME make it to upstream
Patch8: mutt-1.5.23-system_certs.patch
Patch9: mutt-1.9.0-ssl_ciphers.patch
Patch10: mutt-1.9.4-lynx_no_backscapes.patch
Patch12: mutt-1.9.5-nodotlock.patch
Patch13: mutt-1.12.1-optusegpgagent.patch
Patch14: mutt-configure-c99.patch

Url: http://www.mutt.org
Requires: mailcap, urlview
BuildRequires: make
BuildRequires: gcc
BuildRequires: ncurses-devel, gettext, automake
# manual generation
BuildRequires: /usr/bin/xsltproc, docbook-style-xsl, perl-interpreter
BuildRequires: perl-generators
BuildRequires: lynx

%if %{with hcache}
%{?with_tokyocabinet:BuildRequires: tokyocabinet-devel}
%{?with_bdb:BuildRequires: db4-devel}
%{?with_qdbm:BuildRequires: qdbm-devel}
%{?with_gdbm:BuildRequires: gdbm-devel}
%endif

%if %{with imap} || %{with pop} || %{with smtp}
%{?with_gnutls:BuildRequires: gnutls-devel}
%{?with_sasl:BuildRequires: cyrus-sasl-devel}
%endif

%if %{with imap}
%{?with_gss:BuildRequires: krb5-devel}
%endif

%{?with_idn:BuildRequires: libidn-devel}
%{?with_idn2:BuildRequires: libidn2-devel}
%{?with_gpgme:BuildRequires: gpgme-devel}


%description
Mutt is a small but very powerful text-based MIME mail client.  Mutt
is highly configurable, and is well suited to the mail power user with
advanced features like key bindings, keyboard macros, mail threading,
regular expression searches and a powerful pattern matching language
for selecting groups of messages.


%prep
# unpack; cd
%setup -q
# do not run ./prepare -V, because it also runs ./configure

%patch -P 10 -p1 -b .lynx_no_backscapes
%patch -P 12 -p1 -b .nodotlock
%patch -P 14 -p1

autoreconf --install
%patch -P 1 -p1 -b .muttrc
%patch -P 2 -p1 -b .cabundle
%patch -P 3 -p1 -b .syncdebug
%patch -P 8 -p1 -b .system_certs
%patch -P 9 -p1 -b .ssl_ciphers
%patch -P 13 -p1 -b .optusegpgagent

sed -i -r 's/`$GPGME_CONFIG --libs`/"\0 -lgpg-error"/' configure

install -p -m644 %{SOURCE1} mutt_ldap_query

%global hgreldate \\.(201[0-9])([0-1][0-9])([0-3][0-9])hg
if echo %{release} | grep -E -q '%{hgreldate}'; then
  echo -n 'const char *ReleaseDate = ' > reldate.h
  echo %{release} | sed -r 's/.*%{hgreldate}.*/"\1-\2-\3";/' >> reldate.h
fi

# remove mutt_ssl.c to be sure it won't be used because it violates
# Packaging:CryptoPolicies
# https://fedoraproject.org/wiki/Packaging:CryptoPolicies
rm -f mutt_ssl.c


%build
%configure \
    SENDMAIL=%{_sbindir}/sendmail \
    ISPELL=%{_bindir}/hunspell \
    %{?with_debug:	--enable-debug}\
    %{?with_pop:	--enable-pop}\
    %{?with_imap:	--enable-imap} \
    %{?with_smtp:	--enable-smtp} \
\
    %if %{with hcache}
    --enable-hcache \
    %{!?with_tokyocabinet:	--without-tokyocabinet} \
    %{!?with_gdbm:	--without-gdbm} \
    %{!?with_qdbm:	--without-qdbm} \
    %endif
\
    %if %{with imap} || %{with pop} || %{with smtp}
    %{?with_gnutls:	--with-gnutls} \
    %{?with_sasl:	--with-sasl} \
    %endif
\
    %if %{with imap}
    %{?with_gss:	--with-gss} \
    %endif
\
    %{?with_idn:	--with-idn} \
    %{!?with_idn:	--without-idn} \
    %{?with_idn2:   --with-idn2} \
    %{!?with_idn2:  --without-idn2} \
\
    %{?with_gpgme:	--enable-gpgme} \
    %{?with_sidebar: --enable-sidebar} \
    --with-docdir=%{_pkgdocdir}

%make_build

# remove unique id in manual.html because multilib conflicts
sed -i -r 's/<a id="id[a-z0-9]\+">/<a id="id">/g' doc/manual.html

# fix the shebang in mutt_oauth2.py & preserve the time stamp
oauth2_script="contrib/mutt_oauth2.py"
t=$(stat -c %y "${oauth2_script}")
sed -i "s:^#\!/usr/bin/env\s\+python3\s\?$:#!%{python3}:" "${oauth2_script}"
touch -d "$t" "${oauth2_script}"

%install
%make_install

# we like GPG here
cat contrib/gpg.rc >> \
      %{buildroot}%{_sysconfdir}/Muttrc

grep -5 "^color" contrib/sample.muttrc >> \
      %{buildroot}%{_sysconfdir}/Muttrc

cat >> %{buildroot}%{_sysconfdir}/Muttrc <<\EOF
source %{_sysconfdir}/Muttrc.local
EOF

echo "# Local configuration for Mutt." > \
      %{buildroot}%{_sysconfdir}/Muttrc.local

# remove unpackaged files from the buildroot
rm -f %{buildroot}%{_sysconfdir}/*.dist
rm -f %{buildroot}%{_sysconfdir}/mime.types
# disable mutt_dotlock program - remove the compiled binary
rm -f %{buildroot}%{_bindir}/mutt_dotlock
rm -f %{buildroot}%{_bindir}/muttbug
rm -f %{buildroot}%{_bindir}/flea
rm -f %{buildroot}%{_mandir}/man1/mutt_dotlock.1*
rm -f %{buildroot}%{_mandir}/man1/muttbug.1*
rm -f %{buildroot}%{_mandir}/man1/flea.1*
rm -f %{buildroot}%{_mandir}/man5/mbox.5*
rm -f %{buildroot}%{_mandir}/man5/mmdf.5*
rm -rf %{buildroot}%{_pkgdocdir}

# remove /usr/share/info/dir
rm %{buildroot}%{_infodir}/dir

# provide muttrc.local(5): the same as muttrc(5)
ln -sf ./muttrc.5 %{buildroot}%{_mandir}/man5/muttrc.local.5

%find_lang %{name}


%files -f %{name}.lang
%{!?_licensedir:%global license %doc}
%license COPYRIGHT GPL
%doc ChangeLog NEWS README* UPDATING mutt_ldap_query
%doc contrib/mutt_oauth2.py contrib/mutt_oauth2.py.README
%doc contrib/*.rc contrib/sample.* contrib/colors.*
%doc doc/manual.html doc/manual.txt doc/smime-notes.txt
%config(noreplace) %{_sysconfdir}/Muttrc
%config(noreplace) %{_sysconfdir}/Muttrc.local
%{_bindir}/mutt
%{_bindir}/mutt_pgpring
%{_bindir}/pgpewrap
%{_bindir}/smime_keys
%{_mandir}/man1/mutt.*
%{_mandir}/man1/smime_keys.*
%{_mandir}/man1/mutt_pgpring.*
%{_mandir}/man1/pgpewrap.*
%{_mandir}/man5/muttrc.*
%{_infodir}/mutt.info.*


%changelog
* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 5:2.2.13-4
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Fri Aug 09 2024 Michal Hlavinka <mhlavink@redhat.com> - 5:2.2.13-3
- rebuild for updated testing (2x)

* Fri Aug 02 2024 Michal Hlavinka <mhlavink@redhat.com> - 5:2.2.13-1
- update to 2.2.13 (RHEL-52596)
- trim changelog to last 10y

* Mon Jun 24 2024 Ondrej Sloup <osloup@redhat.com> -  5:2.2.12-6
- Remove docbook2X BuildRequire dependency as it is being deprecated
- Resolves: RHEL-45561

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 5:2.2.12-5
- Bump release for June 2024 mass rebuild

* Thu Jan 25 2024 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.2.12-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Sun Jan 21 2024 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.2.12-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Thu Dec  7 2023 Florian Weimer <fweimer@redhat.com> - 5:2.2.12-2
- Fix C99 compatibility issue

* Mon Nov 13 2023 Matej Mužila <mmuzila@redhat.com> - 5:2.2.12-1
- Upgrade to 2.2.12
- Resolves: #2232712

* Tue Sep 05 2023 Matej Mužila <mmuzila@redhat.com> - 5:2.2.11-1
- Upgrade to 2.2.11
- Resolves: #2232712

* Thu Jul 20 2023 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.2.10-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Mon Apr 17 2023 Matej Mužila <mmuzila@redhat.com> - 5:2.2.10-1
- Upgrade to 2.2.10
- Resolves: #2181780

* Thu Jan 19 2023 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.2.9-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Wed Nov 23 2022  Matej Mužila <mmuzila@redhat.com> - 5:2.2.9-1
- Upgrade to 2.2.9
- Resolves: 2140353

* Thu Aug 11 2022  Matej Mužila <mmuzila@redhat.com> - 5:2.2.7-1
- Upgrade to 2.2.7
- Resolves: 2116172

* Fri Jul 22 2022 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.2.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Wed Jun 15 2022  Matej Mužila <mmuzila@redhat.com> - 5:2.2.6-1
- Upgrade to 2.2.6
- Resolves: 2093746

* Mon May 30 2022  Matej Mužila <mmuzila@redhat.com> - 5:2.2.5-1
- Upgrade to 2.2.5
- Resolves: 2068653

* Thu Apr 21 2022 Matej Mužila <mmuzila@redhat.com> - 5:2.2.3-1
- Upgrade to 2.2.3
  Resolves: CVE-2022-1328

* Mon Mar 28 2022 Matej Mužila <mmuzila@redhat.com> - 5:2.2.2-1
- Upgrade to 2.2.2
  Resolves: #2068653

* Tue Feb 22 2022 Matej Mužila <mmuzila@redhat.com> - 5:2.2.1-1
- Upgrade to 2.2.1
  Resolves: #2053874

* Wed Feb 16 2022 Matej Mužila <mmuzila@redhat.com> - 5:2.2.0-1
- Upgrade to 2.2.0
  Resolves: #2053874

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.1.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Mon Jan 03 2022 Matej Mužila <mmuzila@redhat.com> - 5:2.1.5-1
- Upgrade to 2.1.5

* Mon Oct 25 2021 Matej Mužila <mmuzila@redhat.com> - 5:2.1.3-1
- Upgrade to 2.1.3

* Tue Aug 10 2021 Matej Mužila <mmuzila@redhat.com> - 5:2.1.1-1
- Upgrade to 2.1.1

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.0.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Fri May  7 2021 Dan Čermák <dan.cermak@cgc-instruments.com> - 5:2.0.7-2
- Ship the mutt_oauth2.py script as well

* Thu May 6 2021 Filip Januš <fjanus@redhat.com> 5:2.0.7-1
-Rebase to v2.0.7

* Mon Mar 22 2021 Filip Januš <fjanus@redhat.com> 5:2.0.6-1
- Rebase to upstream version 2.0.6

* Mon Feb 1 2021 Filip Januš <fjanus@redhat.com> -5:2.0.5-1
- Rebase to upstream version 2.0.5
- Fix CVE-2021-3181

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5:2.0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Tue Dec 01 2020 Matej Mužila <mmuzila@redhat.com> - 5:2.0.2-1
- Upgrade to 2.0.2
- Resolves: #1895629, #1900827, CVE-2020-28896

* Tue Sep 01 2020 Matej Mužila <mmuzila@redhat.com> - 5:1.14.7-2
- Fix mutt-1.9.4-lynx_no_backscapes.patch

* Mon Aug 31 2020 Matej Mužila <mmuzila@redhat.com> - 5:1.14.7-1
- Upgrade to 1.14.7

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.14.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Wed Jul 22 2020 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.14.6-1
- Upgrade to 1.14.6

* Mon Jul 13 2020 Tom Stellard <tstellar@redhat.com> - 5:1.14.5-2
- Use make macros
- https://fedoraproject.org/wiki/Changes/UseMakeBuildInstallMacro

* Wed Jun 24 2020 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.14.5-1
- Upgrade to 1.14.5

* Fri Jun 19 2020 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.14.4-1
- Upgrade to 1.14.4
- Resolves: #1848768

* Mon Jun 15 2020 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.14.3-1
- Upgrade to 1.14.3
- Resolves: #1836550

* Tue May 26 2020 Matej Mužila <mmuzila@redhat.com> - 5:1.14.2-1
- Upgrade to 1.14.2
- Resolves: #1836550

* Sat May 09 2020 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.14.0-1
- Upgrade to 1.14.0
- Resolves: #1818513

* Tue Apr 07 2020 Matej Mužila <mmuzila@redhat.com> - 5:1.13.5-1
- Upgrade to 1.13.5
- Resolves: #1818513

* Mon Feb 17 2020 Matej Mužila <mmuzila@redhat.com> - 5:1.13.4-1
- Upgrade to 1.13.4
- Resolves: #1803392

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.13.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Wed Jan 15 2020 Matej Mužila <mmuzila@redhat.com> - 5:1.13.3-1
- Upgrade to 1.13.3
- Resolves: #1783717, #1785500

* Fri Jan 03 2020 Matej Mužila <mmuzila@redhat.com> - 5:1.13.2-1
- Upgrade to 1.13.2
- Resolves: #1783717

* Mon Dec 02 2019 Matej Mužila <mmuzila@redhat.com> - 5:1.13.0-1
- Upgrade to 1.13.0
- Resolves: #1754211

* Tue Sep 24 2019 Matej Mužila <mmuzila@redhat.com> - 5:1.12.2-1
- Upgrade to 1.12.2
- Resolves: #1754211

* Thu Aug 29 2019 Matej Mužila <mmuzila@redhat.com> - 5:1.12.1-3
- Make mutt to ask for GPG passphrase
- Resolves: #1731854

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.12.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Wed Jul 17 2019 Matej Mužila <mmuzila@redhat.com> - 5:1.12.1-1
- Upgrade to 1.12.1
- Resolves #1720848

* Tue May 28 2019 Matej Mužila <mmuzila@redhat.com> - 5:1.12.0-1
- Upgrade to 1.12.0
- Resolves #1710398,#1713910

* Wed Apr 24 2019 Björn Esser <besser82@fedoraproject.org> - 5:1.11.4-2
- Remove hardcoded gzip suffix from GNU info pages

* Wed Feb 06 2019 Matej Mužila <mmuzila@redhat.com> - 5:1.11.4-1
- Upgrade to 1.11.4
- Resolves #1688091

* Wed Feb 06 2019 Matej Mužila <mmuzila@redhat.com> - 5:1.11.3-1
- Upgrade to 1.11.3
- Resolves #1659217

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.10.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Tue Jul 17 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.10.1-1
- Upgrade to 1.10.1

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.10.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Thu May 24 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.10.0-2
- Disable post-install mutt_dotlock checking by patch, not by sed.

* Tue May 22 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.10.0-1
- Upgrade to 1.10.0

* Thu Apr 19 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.9.5-4
- Disable dotlock by patch instead of sed
- Resolves: #1568597

* Wed Apr 18 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.9.5-3
- Apply patches of autoreconf related configuration files before running
  autoreconf

* Mon Apr 16 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.9.5-2
- Use libidn2 instead of libidn

* Mon Apr 16 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.9.5-1
- Upgrade to 1.9.5

* Fri Apr 13 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.9.4-2
- Use lynx to generate documentation

* Mon Apr 09 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.9.4-1
- Upgrade to 1.9.4

* Tue Feb 13 2018 Matej Mužila <mmuzila@redhat.com> - 5:1.9.3-1
- Upgrade to 1.9.3

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.9.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Dec 20 2017 Matej Mužila <mmuzila@redhat.com> - 5:1.9.2-1
- Upgrade to 1.9.2

* Sun Sep 24 2017 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.9.1-1
- Upgrade to 1.9.1

* Mon Sep 04 2017 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.9.0-1
- Upgrade to 1.9.0
- Rebase mutt-1.5.23-ssl_ciphers.patch to mutt-1.9.0-ssl_ciphers.patch

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.8.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.8.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Jun 21 2017 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.8.3-1
- Upgrade to 1.8.3

* Mon Apr 24 2017 Matej Mužila <mmuzila@redhat.com> - 5:1.8.2-1
- Update to 1.8.2

* Sat Feb 25 2017 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.8.0-1
- Upgrade to 1.8.0
- Rebase muttrc patch
- Rebase cabundle patch

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.7.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Sat Dec 10 2016 Igor Gnatenko <i.gnatenko.brain@gmail.com> - 5:1.7.2-2
- Rebuild for gpgme 1.18

* Mon Dec 05 2016 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.7.2-1
- Upgrade to 1.7.2

* Sun Oct 09 2016 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.7.1-1
- Upgrade to 1.7.1
- Rebase cabundle patch

* Sat Aug 20 2016 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.7.0-1
- Upgrade to 1.7.0

* Thu Jul 07 2016 Jon Ciesla <limburgher@gmail.com> - 5:1.6.2-1
- Upgrade to 1.6.2

* Mon May 02 2016 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.6.1-1
- Upgrade to 1.6.1

* Mon Apr 18 2016 Fabio Alessandro Locati <fale@fedoraproject.org> - 5:1.6.0-1
- Upgrade to 1.6.0
- Drop patch domainname since it should not be needed any more

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 5:1.5.24-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Sep 04 2015 Matej Muzila <mmuzila@redhat.com> - 5:1.5.24-1
- Updated to 1.5.24 (Resolves: rhbz#1259332)
- Utilize system-wide crypto-policies (Resolves: rhbz#1179324)

* Thu Jun 25 2015 Matej Muzila <mmuzila@redhat.com> - 5:1.5.23-9
- Make system CA bundle default in mutt
- Resolves: #1069778

* Tue Jun 02 2015 Matej Muzila <mmuzila@redhat.com> - 5:1.5.23-8
- Resolves #1227288
- Do not send "From" header without the host part (eg. <foo@>)

* Wed Dec 03 2014 Matej Muzila <mmuzila@redhat.com> - 5:1.5.23-7
- added patch file forgotten in last commit

* Wed Dec 03 2014 Matej Muzila <mmuzila@redhat.com> - 5:1.5.23-6
- resolves #1168464 (CVE-2014-9116)

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5:1.5.23-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5:1.5.23-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon May 26 2014 Jan Pacner <jpacner@redhat.com> - 5:1.5.23-3
- Resolves: #1096756 ([RFE] compile with --enable-debug by default)

* Thu May 15 2014 Dan Horák <dan[at]danny.cz> - 5:1.5.23-2
- update Makefile.am before running autoreconf

